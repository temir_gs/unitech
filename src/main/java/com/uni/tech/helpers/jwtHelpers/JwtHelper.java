package com.uni.tech.helpers.jwtHelpers;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.dtos.UserSignInResponseDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtHelper implements Serializable {

    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Integer getUserIdFromToken(HttpServletRequest request) throws BadRequestException {
        String token = request.getHeader("Authorization").replace("Bearer ", "");
        var claims = getAllClaimsFromToken(token);
        var claimObject = claims.get("userId");

        if (claimObject == null) {
            throw new BadRequestException("UserId not found in token");
        }

        return Integer.parseInt(claimObject.toString());
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateAccessToken(UserSignInResponseDto userSignInResponseDto) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", userSignInResponseDto.getUserId());

        return doGenerateToken(claims, userSignInResponseDto.getUserName());
    }

    public String generateRefreshToken(UserSignInResponseDto userSignInResponseDto) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", userSignInResponseDto.getUserId());

        return doGenerateToken(claims, userSignInResponseDto.getUserName());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    public Boolean validateToken(String token, String userName) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userName) && !isTokenExpired(token));
    }
}
