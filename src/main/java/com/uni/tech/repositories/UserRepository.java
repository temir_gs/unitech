package com.uni.tech.repositories;

import com.uni.tech.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "select *from users where user_name = ?1 and password = ?2 and is_active = true", nativeQuery = true)
    User findByUserNameAndPassword(String userName, String password);

    Boolean existsByUserNameAndIsActive(String userName, Boolean isActive);
}
