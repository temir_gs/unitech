package com.uni.tech.repositories;

import com.uni.tech.dtos.AccountDto;
import com.uni.tech.dtos.AccountListDto;
import com.uni.tech.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    @Query(value = "select account_number as accountNumber, name, balance from accounts where user_id = ?1 and  is_active = true", nativeQuery = true)
    List<AccountListDto> getAllActiveCurrenUserAccounts(Integer userId);

    @Query(value = "select account_number as accountNumber, name from accounts where user_id != ?1 and is_active = true", nativeQuery = true)
    List<AccountDto> getAllActiveAccounts(Integer userId);

    Boolean existsByNameAndIsActive(String name, Boolean isActive);

    Boolean existsByAccountNumberAndIsActive(Integer accountNumber, Boolean isActive);

    Account getByAccountNumber(Integer accountNumber);
}
