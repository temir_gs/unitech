package com.uni.tech.services;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.dtos.UserSignInResponseDto;
import com.uni.tech.dtos.UserCreateDto;
import com.uni.tech.dtos.UserSignInDto;
import com.uni.tech.models.User;
import com.uni.tech.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;

    @Override
    public void signUp(UserCreateDto userCreateDto) throws BadRequestException {

        Boolean isExist = isUserNameValid(userCreateDto.getUserName());
        if (isExist) {
            throw new BadRequestException("UserName  is exist!");
        }

        User user = new User();
        user.setName(userCreateDto.getName());
        user.setSurname(userCreateDto.getSurname());
        user.setUserName(userCreateDto.getUserName());
        user.setPassword(userCreateDto.getPassword());
        user.setIsActive(true);

        userRepository.save(user);
    }

    @Override
    public UserSignInResponseDto signIn(UserSignInDto userSignInDto) throws BadRequestException {
        User user = userRepository.findByUserNameAndPassword(
                userSignInDto.getUserName(),
                userSignInDto.getPassword());

        if (user == null) {
            throw new BadRequestException("UserName or Password is invalid!");
        }

        UserSignInResponseDto userSignInResponseDto = new UserSignInResponseDto();
        userSignInResponseDto.setUserId(user.getId());
        userSignInResponseDto.setUserName(user.getUserName());

        return userSignInResponseDto;
    }

    @Override
    public Boolean isUserNameValid(String userName) {
        Boolean isExist = userRepository.existsByUserNameAndIsActive(userName, true);
        return isExist;
    }
}
