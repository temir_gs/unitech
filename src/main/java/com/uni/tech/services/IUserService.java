package com.uni.tech.services;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.dtos.UserSignInResponseDto;
import com.uni.tech.dtos.UserCreateDto;
import com.uni.tech.dtos.UserSignInDto;

public interface IUserService {
    void signUp(UserCreateDto userCreateDto) throws BadRequestException;

    UserSignInResponseDto signIn(UserSignInDto userSignInDto) throws BadRequestException;

    Boolean isUserNameValid(String userName);
}
