package com.uni.tech.services;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.common.exceptions.NotFoundException;
import com.uni.tech.dtos.AccountCreateDto;
import com.uni.tech.dtos.AccountDto;
import com.uni.tech.dtos.AccountListDto;
import com.uni.tech.dtos.AccountTransferDto;
import com.uni.tech.helpers.jwtHelpers.JwtHelper;
import com.uni.tech.models.Account;
import com.uni.tech.models.User;
import com.uni.tech.repositories.AccountRepository;
import com.uni.tech.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService implements IAccountService {
    private final JwtHelper jwtHelper;
    private final HttpServletRequest request;
    private final UserRepository userRepository;
    private final AccountRepository accountRepository;


    @Override
    public void create(AccountCreateDto accountCreateDto) throws BadRequestException {
        Boolean nameIsExist = isNameValid(accountCreateDto.getName());
        Boolean accountNumberIsExist = isAccountNumberValid(accountCreateDto.getAccountNumber());

        if (nameIsExist) {
            throw new BadRequestException("Account name is exist!");
        }

        if (accountNumberIsExist) {
            throw new BadRequestException("Account number is exist!");
        }

        int userId = jwtHelper.getUserIdFromToken(request);
        User user = userRepository.getById(userId);

        Account account = new Account();
        account.setIsActive(true);
        account.setName(accountCreateDto.getName());
        account.setBalance(accountCreateDto.getBalance());
        account.setAccountNumber(accountCreateDto.getAccountNumber());
        account.setUser(user);

        accountRepository.save(account);
    }

    @Override
    public void increase(Integer accountNumber, double amount) throws NotFoundException {
        Account account = accountRepository.getByAccountNumber(accountNumber);

        if (account == null) {
            throw new NotFoundException("No account with AccountNumber: {accountNumber} was found"
                    .replace("{accountNumber}", accountNumber.toString()));
        }

        Double currentBalance = account.getBalance();

        account.setBalance(currentBalance + amount);
        accountRepository.save(account);
    }

    @Override
    public List<AccountListDto> getAllActiveCurrenUserAccounts() throws BadRequestException {
        int userId = jwtHelper.getUserIdFromToken(request);

        List<AccountListDto> accounts = accountRepository.getAllActiveCurrenUserAccounts(userId);
        return accounts;
    }

    @Override
    public List<AccountDto> getAllActiveAccounts() throws BadRequestException {
        int userId = jwtHelper.getUserIdFromToken(request);
        List<AccountDto> accounts = accountRepository.getAllActiveAccounts(userId);
        return accounts;
    }

    @Override
    public Boolean isNameValid(String name) {
        Boolean isExist = accountRepository.existsByNameAndIsActive(name, true);
        return isExist;
    }

    @Override
    public Boolean isAccountNumberValid(Integer accountNumber) {
        Boolean isExist = accountRepository.existsByAccountNumberAndIsActive(accountNumber, true);
        return isExist;
    }

    @Override
    public void activate(Integer accountNumber) throws NotFoundException, BadRequestException {
        Account account = accountRepository.getByAccountNumber(accountNumber);

        if (account == null) {
            throw new NotFoundException("No account with AccountNumber: {accountNumber} was found"
                    .replace("{accountNumber}", accountNumber.toString()));
        }

        if (account.getIsActive()) {
            throw new BadRequestException("Account with AccountNumber: {accountNumber} already activated"
                    .replace("{accountNumber}", accountNumber.toString()));
        }

        account.setIsActive(true);
        accountRepository.save(account);
    }

    @Override
    public void deActivate(Integer accountNumber) throws NotFoundException, BadRequestException {
        Account account = accountRepository.getByAccountNumber(accountNumber);

        if (account == null) {
            throw new NotFoundException("No account with AccountNumber: {accountNumber} was found"
                    .replace("{accountNumber}", accountNumber.toString()));
        }

        if (!account.getIsActive()) {
            throw new BadRequestException("Account with AccountNumber: {accountNumber} already deActivated"
                    .replace("{accountNumber}", accountNumber.toString()));
        }

        account.setIsActive(false);
        accountRepository.save(account);
    }

    @Override
    @Transactional
    public void makeTransfer(AccountTransferDto accountTransferDto) throws NotFoundException, BadRequestException {
        Account senderAccount = accountRepository.getByAccountNumber(accountTransferDto.getSenderAccountNumber());

        if (senderAccount == null) {
            throw new NotFoundException("No account with AccountNumber: {AccountNumber} was found"
                    .replace("{AccountNumber}", accountTransferDto.getSenderAccountNumber().toString()));
        }

        if (!senderAccount.getIsActive()) {
            throw new BadRequestException("Account with AccountNumber: {AccountNumber} is deActivated. You cannot make transfer!!"
                    .replace("{AccountNumber}", accountTransferDto.getSenderAccountNumber().toString()));
        }

        if (accountTransferDto.getAmount() > senderAccount.getBalance()) {
            throw new BadRequestException("You don't have enough funds in your balance!!");
        }

        Account receivingAccount = accountRepository.getByAccountNumber(accountTransferDto.getReceivingAccountNumber());

        if (receivingAccount == null) {
            throw new NotFoundException("No account with AccountNumber: {AccountNumber} was found"
                    .replace("{AccountNumber}", accountTransferDto.getReceivingAccountNumber().toString()));
        }

        if (!receivingAccount.getIsActive()) {
            throw new BadRequestException("Account with AccountNumber: {AccountNumber} is deActivated. You cannot make transfer!!"
                    .replace("{AccountNumber}", accountTransferDto.getReceivingAccountNumber().toString()));
        }

        senderAccount.setBalance(senderAccount.getBalance() - accountTransferDto.getAmount());
        accountRepository.save(senderAccount);

        receivingAccount.setBalance(receivingAccount.getBalance() + accountTransferDto.getAmount());
        accountRepository.save(receivingAccount);
    }
}
