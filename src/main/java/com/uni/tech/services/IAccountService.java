package com.uni.tech.services;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.common.exceptions.NotFoundException;
import com.uni.tech.dtos.AccountCreateDto;
import com.uni.tech.dtos.AccountDto;
import com.uni.tech.dtos.AccountListDto;
import com.uni.tech.dtos.AccountTransferDto;

import java.util.List;

public interface IAccountService {
    void create(AccountCreateDto accountCreateDto) throws BadRequestException;

    void increase(Integer accountNumber, double amount) throws NotFoundException;

    List<AccountListDto> getAllActiveCurrenUserAccounts() throws BadRequestException;

    List<AccountDto> getAllActiveAccounts() throws BadRequestException;

    Boolean isNameValid(String name);

    Boolean isAccountNumberValid(Integer accountNumber);

    void activate(Integer accountNumber) throws NotFoundException, BadRequestException;
    void deActivate(Integer accountNumber) throws NotFoundException, BadRequestException;

    void makeTransfer(AccountTransferDto accountTransferDto) throws NotFoundException, BadRequestException;
}
