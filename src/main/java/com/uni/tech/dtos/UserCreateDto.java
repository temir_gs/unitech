package com.uni.tech.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserCreateDto {
    @NotBlank(message = "Name is required!")
    private String name;
    @NotBlank(message = "Surname is required!")
    private String surname;
    @NotBlank(message = "UserName is required!")
    private String userName;
    @NotBlank(message = "Password is required!")
    private String password;
}
