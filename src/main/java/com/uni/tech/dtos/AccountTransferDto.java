package com.uni.tech.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountTransferDto {
    @NotBlank(message = "Amount is required!")
    private Double amount;
    @NotBlank(message = "SenderAccountNumber is required!")
    private Integer senderAccountNumber;
    @NotBlank(message = "ReceivingAccountNumber is required!")
    private Integer receivingAccountNumber;
}
