package com.uni.tech.dtos;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSignInResponseDto {
    private int userId;
    private String userName;
}
