package com.uni.tech.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AccountCreateDto {
    @NotBlank(message = "Name is required!")
    private String name;
    private Double balance;
    @NotBlank(message = "AccountNumber is required!")
    private Integer accountNumber;
}
