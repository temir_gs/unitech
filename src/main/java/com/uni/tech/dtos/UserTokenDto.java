package com.uni.tech.dtos;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTokenDto {
    private String accessToken;
    private String RefreshToken;
}
