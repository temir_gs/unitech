package com.uni.tech.dtos;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSignInDto {
    @NotBlank(message = "UserName is required!")
    private String userName;
    @NotBlank(message = "Password is required!")
    private String password;
}
