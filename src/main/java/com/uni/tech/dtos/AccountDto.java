package com.uni.tech.dtos;

public interface AccountDto {
    String getName();
    Integer getAccountNumber();
}
