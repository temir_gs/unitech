package com.uni.tech.dtos;

public interface AccountListDto {
    Integer getAccountNumber();

    String getName();

    Double getBalance();
}
