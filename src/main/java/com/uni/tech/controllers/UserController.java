package com.uni.tech.controllers;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.dtos.UserCreateDto;
import com.uni.tech.dtos.UserSignInDto;
import com.uni.tech.dtos.UserSignInResponseDto;
import com.uni.tech.dtos.UserTokenDto;
import com.uni.tech.helpers.jwtHelpers.JwtHelper;
import com.uni.tech.services.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/uni-tech/")
public class UserController {

    private final JwtHelper jwtHelper;
    private final IUserService userService;
    private final HttpServletRequest request;

    @PostMapping("users/sign-up")
    public void signUp(@RequestBody UserCreateDto userCreateDto) throws BadRequestException {
        userService.signUp(userCreateDto);
    }

    @PostMapping("users/sign-in")
    public ResponseEntity<UserTokenDto> signIn(@RequestBody UserSignInDto userSignInDto) throws BadRequestException {
        UserSignInResponseDto userSignInResponseDto = userService.signIn(userSignInDto);

        UserTokenDto userTokenDto = new UserTokenDto();
        userTokenDto.setAccessToken(jwtHelper.generateAccessToken(userSignInResponseDto));
        userTokenDto.setRefreshToken(jwtHelper.generateRefreshToken(userSignInResponseDto));

        return ResponseEntity.ok(userTokenDto);
    }
}
