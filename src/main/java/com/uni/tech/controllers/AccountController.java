package com.uni.tech.controllers;

import com.uni.tech.common.exceptions.BadRequestException;
import com.uni.tech.common.exceptions.NotFoundException;
import com.uni.tech.dtos.AccountCreateDto;
import com.uni.tech.dtos.AccountDto;
import com.uni.tech.dtos.AccountListDto;
import com.uni.tech.dtos.AccountTransferDto;
import com.uni.tech.services.IAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/uni-tech/")
public class AccountController {
    private final IAccountService accountService;

    @PostMapping("accounts")
    public void create(@RequestBody AccountCreateDto accountCreateDto) throws BadRequestException {
        accountService.create(accountCreateDto);
    }

    @PatchMapping("accounts/{accountNumber}/increase")
    public void increase(@PathVariable Integer accountNumber, @RequestParam Double amount) throws NotFoundException {
        accountService.increase(accountNumber, amount);
    }

    @GetMapping("accounts/current-user/all")
    public ResponseEntity<List<AccountListDto>> getAllActiveCurrenUserAccounts() throws BadRequestException {
        List<AccountListDto> accounts = accountService.getAllActiveCurrenUserAccounts();

        return ResponseEntity.ok(accounts);
    }

    @GetMapping("accounts/all")
    public ResponseEntity<List<AccountDto>> getAllActiveAccounts() throws BadRequestException {
        List<AccountDto> accounts = accountService.getAllActiveAccounts();

        return ResponseEntity.ok(accounts);
    }

    @PatchMapping("accounts/{accountNumber}/activate")
    public void activate(@PathVariable Integer accountNumber) throws NotFoundException, BadRequestException {
        accountService.activate(accountNumber);
    }

    @PatchMapping("accounts/{accountNumber}/deActivate")
    public void deActivate(@PathVariable Integer accountNumber) throws NotFoundException, BadRequestException {
        accountService.deActivate(accountNumber);
    }

    @PutMapping("accounts/transfer")
    public void makeTransfer(@RequestBody AccountTransferDto accountTransferDto) throws NotFoundException, BadRequestException {
        accountService.makeTransfer(accountTransferDto);
    }
}
