### UniTech REST API

This project is written in Spring Boot with Java 11. 

### Docker Compose

To run docker compose of this repo run the following command:
```
docker-compose up --build -d  
```

This command will prepare image with name `unitech_web.api`.

Now, you can navigate your browser to http://localhost:8081/swagger-ui.html to see the Swagger documentation of this project.
