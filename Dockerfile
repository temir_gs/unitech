FROM openjdk:11-jdk
USER root:root

ARG DB_SERVER
ARG DB_PORT
ARG DB_NAME
ARG DB_USER
ARG DB_PASSWORD



ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/app.jar"]

#CMD ["/app.jar"]
